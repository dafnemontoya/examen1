package com.example.examencorteuno;

import java.io.Serializable;

public class ReciboNomina implements Serializable {
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;

    public ReciboNomina() {
    }

    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }



    public float calcularSubtotal()  {
        float subtotal = 0;

        //Auxiliar = 1
        if (this.puesto == 1) {
            subtotal = (this.horasTrabNormal * 50) + (this.horasTrabExtras * 100);
        }

        //Albañil = 2
        else if (this.puesto == 2) {
            subtotal = (this.horasTrabNormal * 70) + (this.horasTrabExtras * 140);
        }

        //Ing. Obra = 3
        else if (this.puesto == 3) {
            subtotal = (this.horasTrabNormal * 100) + (this.horasTrabExtras * 200);
        }

        return subtotal;
    }

    public float calcularImpuesto()  {
        return (float) (calcularSubtotal() * 0.16);
    }

    public float calcularTotal()  {
        return calcularSubtotal() - calcularImpuesto();
    }

}
