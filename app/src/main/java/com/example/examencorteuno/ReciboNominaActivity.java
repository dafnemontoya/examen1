package com.example.examencorteuno;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ReciboNominaActivity extends AppCompatActivity {
    private ReciboNomina recibo;
    private EditText numRecibo;
    private TextView lblNombre;
    private EditText txtHorasTrab;
    private EditText txtHorasExtras;
    private TextView lblImpuesto;
    private TextView lblSubtotal;
    private TextView lblTotal;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private RadioGroup grupo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        numRecibo = (EditText) findViewById(R.id.txtRecibo);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        txtHorasTrab = (EditText) findViewById(R.id.txtHorasTrab);
        txtHorasExtras = (EditText) findViewById(R.id.txtHorasExtra);
        lblSubtotal = (TextView) findViewById(R.id.lblSubtotal);
        lblImpuesto = (TextView) findViewById(R.id.lblImpuesto);
        lblTotal = (TextView) findViewById(R.id.lblTotal);
        grupo = (RadioGroup) findViewById(R.id.grpPuestos);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nom");
        lblNombre.setText(nombre);

        recibo = new ReciboNomina();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numRecibo.getText().toString().matches("") || txtHorasTrab.getText().toString().matches("")
                        || txtHorasExtras.getText().toString().matches("")) {
                    Toast.makeText(ReciboNominaActivity.this, "Favor de llenar todos los campos", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (grupo.getCheckedRadioButtonId() == R.id.rdbAuxiliar) {
                        recibo.setPuesto(1);
                    } else if (grupo.getCheckedRadioButtonId() == R.id.rdbAlbanil) {
                        recibo.setPuesto(2);
                    } else if (grupo.getCheckedRadioButtonId() == R.id.rdbIngObra) {
                        recibo.setPuesto(3);
                    }

                    recibo.setHorasTrabNormal(Float.parseFloat(txtHorasTrab.getText().toString()));
                    recibo.setHorasTrabExtras(Float.parseFloat(txtHorasExtras.getText().toString()));

                    String sub = "$"+recibo.calcularSubtotal();
                    String imp = "$"+recibo.calcularImpuesto();
                    String tot = "$"+recibo.calcularTotal();

                    lblSubtotal.setText(sub);
                    lblImpuesto.setText(imp);
                    lblTotal.setText(tot);
                }
            }
        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtHorasTrab.setText("");
                txtHorasExtras.setText("");
                numRecibo.setText("");
                lblSubtotal.setText("");
                lblImpuesto.setText("");
                lblTotal.setText("");
                RadioButton primero = (RadioButton) grupo.getChildAt(0);
                primero.setChecked(true);
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
